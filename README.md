External ray on the parameter plane traced outward ( from point c ( from exterior of Mandelbrot set ) to the infinity )
using Newton method




# code
* original c code of m-exray-out procedure from [the mandelbrot-numerics library](https://code.mathr.co.uk/mandelbrot-numerics) by  [Claude Heiland-Allen](http://mathr.co.uk/)
  * file with main procedure [m-exray-out.c](https://code.mathr.co.uk/mandelbrot-numerics/blob/HEAD:/c/bin/m-exray-out.c)
  * [file with library functions](https://code.mathr.co.uk/mandelbrot-numerics/blob_plain/HEAD:/c/lib/m_d_exray_out.c)
* one file ( stand alone program) unofficial version: 
  * [m.c](m.c) - without modifications
  * [n.c](n.c) - with modifications 

# Description
## General description
* [unofficial wiki](https://en.wikibooks.org/wiki/Fractals/mandelbrot-numerics#m-exray-out)
* [wikipedia : external ray](https://en.wikipedia.org/wiki/External_ray)


## Dictionary

### dwell 
* dwell = ray->d
* dwell function computes dwell ( returns double ) 
* [maxdwell](http://www.mrob.com/pub/muency/dwelllimit.html)  = max dwell integer constant



maxdwell constant is used as a limit for the iterations: 

```c
for (int i = 0; i < maxdwell; ++i) 
```

dwell function: 

```c
// from  m_d_exray_out.c 
static double dwell(double loger2, int n, double zmag2) {
  return n - log2(log(zmag2) / loger2);
}

```

from  m_d_exray_out_step: 
```c 
ray->d -= 1.0 / ray->sharpness;
if (ray->d <= 0)     return m_converged;
```



dwell is used for computing 
```c
// from m_d_exray_out_step
int m = ceil(ray->d);
double r = pow(ray->er, pow(2, m - ray->d));
double a = carg(ray->z) / twopi;
double t = a - floor(a);
```


## Algorithm

Newton's Method: Ray Out

>>>
you need to trace a ray outwards, which means using different C values, and the bits come in reverse order, first the deepest bit from the
iteration count of the start pixel, then move C outwards along the ray (perhaps using the newton's method of mandel-exray.pdf in reverse),
repeat until no more bits left.  you move C a fractional iteration count 
each time, and collect bits when crossing integer dwell boundaries
>>>


[Animation made by Claude](https://mathr.co.uk/mandelbrot/2015-03-04_exray_binary.gif) showing how the bits are collected ( in the reverse order) when tracing a ray outwards:
* At the bootom of the image there is a boundary of Mandlebrot set
* at the top of the image is the escape radius ( "infinity")
* [the most significant bit (MSB, also called the high-order bit) is the bit position in a binary number having the greatest value.](https://en.wikipedia.org/wiki/Bit_numbering#Most_significant_bit) 
  * It is computed as a last bit, when ray is crossing circle with radius = er
  
  

![image by Claude from https://mathr.co.uk/mandelbrot/2015-03-04_exray_binary.gif](2015-03-04_exray_binary.gif) 



The **itinerary** of angle $`\theta`$  is its **binary expansion**. See [Emily Allaway: The Mandelbrot Set and the Farey Tree]( https://sites.math.washington.edu/~morrow/336_18/2016papers/emily.pdf)



More description:
* [old fractalforum : binary-decomposition-and-external-angles](http://www.fractalforums.com/animations-showcase-(rate-my-short-animation)/binary-decomposition-and-external-angles/)
* [old fractalforum : smooth-external-angle-of-mandelbrot-set/](http://www.fractalforums.com/programming/smooth-external-angle-of-mandelbrot-set/)
* 

When crossing dwell bands, compute the doubling preimages of $`\theta`$:  
* $`\theta_-`$
* $`\theta_+`$

using equations:  
$`\theta_- = \frac{\theta}{2}`$  
$`\theta_+ = \frac{\theta + 1}{2}`$  

Then compute:
* $`r_{-}`$ 
* $`r_{+}`$   
using Newton's method.  

Choose the nearest to $`r_m`$ to be $`r_{m+1}`$.  

Collect a list of which choices were made to determine the final $`\theta`$ in high precision without relying on the bits of $`\theta`$ itself.  

$`\lambda > 1`$ controls the sharpness of the ray.  

Stop tracing when r reaches the escape radius.


Demo by Claude showing all actions [original site](https://mathr.co.uk/mandelbrot/2015-02-27_morph_demo.mkv)

# examples outputs:


## Wolf Jung test 
Test from [the program Mandel](http://www.mndynamics.com/indexp.html) by Wolf Jung : 

  the angles 1/7 (of period 3) and 321685687669320/2251799813685247 (of period 51) differ by about 10-15, but the landing points of the corresponding parameter rays are about 0.035 apart  

This image shows:
* part of parameter plan, interior is black and exterior is coloured with binary decomposition, so level sets of escape time are seen
* rays 1/7 and point c = -0.091485351562500  +0.652061279296875 *I lying on this ray
* ray 321685687669320/2251799813685247 and point c = -0.089034667968750  +0.651030761718750*I lying on this ray


![j_test.png](j_test.png)


## c = -0.1245993268654540; 0.6495194127102700

This point is "on" the external ray 1/7 = 0.(001)  

Informations from [the program Mandel](http://www.mndynamics.com/indexp.html) by Wolf Jung : 

>>>
The angle  1/7  or  p001
has  preperiod = 0  and  period = 3.
The conjugate angle is  2/7  or  p010 .
The kneading sequence is  AA*  and
the internal address is  1-3 .
The corresponding parameter rays are landing
at the root of a satellite component of period 3.
It is bifurcating from period 1.
>>>

Output of my [n.c](n.c) code : 

```bash
external angle of ray thru point c = -0.1245993268654540; 0.6495194127102700 as
- decimal fraction  = 0.1428571428571428 
- unformatted binary fraction  = 	0.00100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100100

aproximated with double precision numbers 

escape time n = 4526 < Depth = D = maxdwell = 4600  for escape radius = 20

real	0m3,383s
user	0m3,383s
sys	0m0,000s

```


# binary fraction 
* [Check if a string is entirely made of the same substring](https://codegolf.stackexchange.com/questions/184682/check-if-a-string-is-entirely-made-of-the-same-substring)
* [rep-string](https://rosettacode.org/wiki/Rep-string): 

## rep-string
>>>
Given a series of ones and zeroes in a string, define a repeated string or rep-string as a string which is created by repeating a substring of the first N characters of the string truncated on the right to the length of the input string, and in which the substring appears repeated at least twice in the original.

For example, the string 10011001100 is a rep-string as the leftmost four characters of 1001 are repeated three times and truncated on the right to give the original string.

Note that the requirement for having the repeat occur two or more times means that the repeating unit is never longer than half the length of the input string.


>>>




# Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc

# License

This project is licensed under [License GPL3+]( http://www.gnu.org/licenses/gpl.html)

# technical notes
GitLab uses:
* the Redcarpet Ruby library for [Markdown processing](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md) see also [user doc](https://docs.gitlab.com/ee/user/markdown.html)
* KaTeX to render [math written with the LaTeX syntax](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md), but [only subset](https://khan.github.io/KaTeX/function-support.html)




## Git
```
cd existing_folder
git init
git remote add origin git@gitlab.com:adammajewski/mandelbrot-ex_ray-out.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

Local repo: ~/c/mandel/m-exray-out-d/m 

