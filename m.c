/*

code from mandelbrot-numerics library 
numerical algorithms related to the Mandelbrot set
Copyright (C) 2015-2017 Claude Heiland-Allen
http://mathr.co.uk/

License GPL3+ http://www.gnu.org/licenses/gpl.html

https://code.mathr.co.uk/mandelbrot-numerics

https://en.wikibooks.org/wiki/Fractals/mandelbrot-numerics



m-exray-out 
https://en.wikibooks.org/wiki/Fractals/mandelbrot-numerics#m-exray-out

gcc m.c -lm -Wall



usage: ./a.out precision c-re c-im sharpness maxdwell preperiod period

-----------------
double -1.7904997268969142e-01  1.0856197533070304e+00 4  20 100 1   // .010000100100100






./a.out double -1.7904997268969142e-01  1.0856197533070304e+00 4  20 100 1 
-1.7904997268969142e-01 1.0856197533070304e+00
-1.7935443511056054e-01 1.0854961235734493e+00
-1.7969015435633140e-01 1.0853555826931407e+00
-1.8051917357961708e-01 1.0850119514784207e+00 1
-1.8269681168554189e-01 1.0840986080730519e+00
-1.8930978756749961e-01 1.0809967205891908e+00 0
-1.9360134634853554e-01 1.0737664210007076e+00 1
-2.0801776265667046e-01 1.0704315884719302e+00 0
-2.4370145596832851e-01 1.0627226337492655e+00 1
-2.7278498707172455e-01 1.0400463032627758e+00 1
-3.6142246174842857e-01 9.5627327660306660e-01
-4.0875942404913329e-01 9.4318016425218687e-01
-4.5809426255241681e-01 9.9407738931502432e-01 1
-6.0223759082842288e-01 9.3674887784190841e-01
-8.0664626534435835e-01 8.8455840042031386e-01 0
-8.3743553612789834e-01 9.3674304615806725e-01
-8.7287981833135664e-01 9.9952074265609137e-01
-9.1381642842183941e-01 1.0750773818725967e+00
-9.6138941844723436e-01 1.1665050307228815e+00 0
-1.0172147464760770e+00 1.2782611560409487e+00
-1.0836516873794186e+00 1.4168346080502183e+00
-1.1642543320399381e+00 1.5917680433570660e+00
-1.2645279518271537e+00 1.8173272090846146e+00 0
-1.3932089082214374e+00 2.1153654197070240e+00
-1.5644798765083927e+00 2.5204286897782238e+00
-1.8019543103478257e+00 3.0891768094384502e+00
-2.1462162170666716e+00 3.9184694463323830e+00 1
-2.6699272827383060e+00 5.1817748128576184e+00
-3.5099957053533952e+00 7.2066815445348764e+00
-4.9407140183321250e+00 1.0650884836446950e+01
-7.5526171722332034e+00 1.6932133257514206e+01 0
-1.2727799761990893e+01 2.9370322256332429e+01
-2.4030360674257757e+01 5.6528056606204473e+01
-5.1753810887543764e+01 1.2313571132413860e+02
-1.2986129881788275e+02 3.1079020626913712e+02 1
-3.8951035577778663e+02 9.3459826885312373e+02
-1.4412515103402823e+03 3.4614095793299703e+03
-6.8365543576278487e+03 1.6423640211807055e+04
-4.3547867632624519e+04 1.0462387704049867e+05 0
-3.9380510085478675e+05 9.4611788566395245e+05
-5.4017847803638447e+06 1.2977803446720989e+07
-1.2161023621086851e+08 2.9216894171553040e+08

.01010001110101()

------------------------------------------------- 


cd existing_folder
git init
git remote add origin git@gitlab.com:adammajewski/mandelbrot-ex_ray-out.git
git add .
git commit -m "Initial commit"
git push -u origin master




*/





#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h> // fprintf
#include <string.h> // strcmp
#include <errno.h>  // errno




enum m_newton { m_failed, m_stepped, m_converged };
typedef enum m_newton m_newton;





// ------------c/lib/m_d_util.h ------------------------------------------------

static inline int sgn(double z) {
  if (z > 0) { return  1; }
  if (z < 0) { return -1; }
  return 0;
}

static inline bool odd(int a) {
  return a & 1;
}

static inline double cabs2(double _Complex z) {
  return creal(z) * creal(z) + cimag(z) * cimag(z);
}

static inline bool cisfinite(double _Complex z) {
  return isfinite(creal(z)) && isfinite(cimag(z));
}

static const double pi = 3.141592653589793;
static const double twopi = 6.283185307179586;

// last . takeWhile (\x -> 2 /= 2 + x) . iterate (/2) $ 1 :: Double
static const double epsilon = 4.440892098500626e-16;

// epsilon^2
static const double epsilon2 = 1.9721522630525295e-31;


//----------------- m_d_exray_out.c ----------------------------------------------------


static double dwell(double loger2, int n, double zmag2) {
  return n - log2(log(zmag2) / loger2);
}



// ---------------- m-util.h --------------------------


static inline bool arg_precision(const char *arg, bool *native, int *bits) {
  if (0 == strcmp("double", arg)) {
    *native = true;
    *bits = 53; // double  =  native = 53 bits 
    return true;
  } else {
    char *check = 0;
    errno = 0;
    long int li = strtol(arg, &check, 10);
    bool valid = ! errno && arg != check && ! *check;
    int i = li;
    if (valid && i > 1) {
      *native = false;
      *bits = i;
      return true;
    }
  }
  return false;
}

static inline bool arg_double(const char *arg, double *x) {
  char *check = 0;
  errno = 0;
  double d = strtod(arg, &check);
  if (! errno && arg != check && ! *check) {
    *x = d;
    return true;
  }
  return false;
}

static inline bool arg_int(const char *arg, int *x) {
  char *check = 0;
  errno = 0;
  long int li = strtol(arg, &check, 10);
  if (! errno && arg != check && ! *check) {
    *x = li;
    return true;
  }
  return false;
}




// ============================ m_d_exray_out ===========================

struct m_d_exray_out {
  int sharpness;
  double er;
  double er2;
  double loger2;
  double _Complex c;
  double _Complex z;
  double d;
  int n;
  int bit;
};


typedef struct m_d_exray_out m_d_exray_out;

m_d_exray_out *m_d_exray_out_new(double _Complex c, int sharpness, int maxdwell) {
  double er = 65536;
  double er2 = er * er;
  int n = 0;
  double _Complex z = 0;
  for (int i = 0; i < maxdwell; ++i) {
    n = n + 1;
    z = z * z + c;
    if (cabs2(z) > er2) {
      break;
    }
  }
  if (! (cabs2(z) > er2)) {
    return 0;
  }
  m_d_exray_out *ray = malloc(sizeof(*ray));
  if (! ray) {
    return 0;
  }
  ray->sharpness = sharpness;
  ray->er = er;
  ray->er2 = er2;
  ray->loger2 = log(er2);
  ray->c = c;
  ray->z = z;
  ray->d = dwell(ray->loger2, n, cabs2(z));
  ray->n = n;
  ray->bit = -1;
  return ray;
}

void m_d_exray_out_delete(m_d_exray_out *ray) {
  if (ray) {
    free(ray);
  }
}

m_newton m_d_exray_out_step(m_d_exray_out *ray) {
  if (! ray) {
    return m_failed;
  }
  ray->bit = -1;
  ray->d -= 1.0 / ray->sharpness;
  if (ray->d <= 0) {
    return m_converged;
  }
  
  
  int m = ceil(ray->d);
  double r = pow(ray->er, pow(2, m - ray->d));
  double a = carg(ray->z) / twopi;
  double t = a - floor(a);
  
  
  
  if (m == ray->n) {
    double _Complex k = r * cexp(I * twopi *  t);
    double _Complex c = ray->c;
    double _Complex z = 0;
    
    
    
    for (int i = 0; i < 64; ++i) { // FIXME arbitrary limit
      double _Complex dc = 0;
      z = 0;
      for (int p = 0; p < m; ++p) {
        dc = 2 * z * dc + 1;
        z = z * z + c;
      }
      double _Complex c_new = c - (z - k) / dc;
      double d2 = cabs2(c_new - c);
      if (cisfinite(c_new)) {
        c = c_new;
      } else {
        break;
      }
      if (d2 <= epsilon2) {
        break;
      }
    }
    
    
    if (cisfinite(c)) {
      ray->c = c;
      ray->z = z;
      ray->d = dwell(ray->loger2, m, cabs2(z));
      return m_stepped;
    }
    
    return m_failed;
  } //   if (m == ray->n)
  
    else {
    double _Complex k[2] = { r * cexp(I * pi * t), r * cexp(I * pi * (t + 1)) };
    double _Complex c[2] = { ray->c, ray->c };
    double _Complex z[2] = { 0, 0 };
    double d2[2];
    double e2[2];
    for (int i = 0; i < 64; ++i) { // FIXME arbitrary limit
      z[0] = 0;
      z[1] = 0;
      double _Complex dc[2] = { 0, 0 };
      for (int p = 0; p < m; ++p) {
        for (int w = 0; w < 2; ++w) {
          dc[w] = 2 * z[w] * dc[w] + 1;
          z[w] = z[w] * z[w] + c[w];
        }
      }
      double _Complex c_new[2];
      for (int w = 0; w < 2; ++w) {
        c_new[w] = c[w] - (z[w] - k[w]) / dc[w];
        e2[w] = cabs2(c_new[w] - c[w]);
        d2[w] = cabs2(c_new[w] - ray->c);
        c[w] = c_new[w];
      }
      if (! (e2[0] > epsilon2 && e2[1] > epsilon2)) {
        break;
      }
    }
    if ((cisfinite(c[0]) && cisfinite(c[1]) && d2[0] <= d2[1])
     || (cisfinite(c[0]) && ! cisfinite(c[1]))) {
      ray->bit = 0;
      ray->c = c[0];
      ray->z = z[0];
      ray->n = m;
      ray->d = dwell(ray->loger2, m, cabs2(z[0]));
      return m_stepped;
    }
    if ((cisfinite(c[0]) && cisfinite(c[1]) && d2[1] <= d2[0])
     || (! cisfinite(c[0]) && cisfinite(c[1]))) {
      ray->bit = 1;
      ray->c = c[1];
      ray->z = z[1];
      ray->n = m;
      ray->d = dwell(ray->loger2, m, cabs2(z[1]));
      return m_stepped;
    }
    return m_failed;
  } // if (m == ray->n) ... else 
  
  
} // m_newton m_d_exray_out_step




bool m_d_exray_out_have_bit(const m_d_exray_out * ray) {
  if (! ray) {
    return false;
  }
  return 0 <= ray->bit;
}

bool m_d_exray_out_get_bit(const m_d_exray_out *ray) {
  if (! ray) {
    return false;
  }
  return ray->bit;
}

double complex m_d_exray_out_get(const m_d_exray_out *ray) {
  if (! ray) {
    return 0;
  }
  return ray->c;
}



char *m_d_exray_out_do(double complex c, int sharpness, int maxdwell) {
  m_d_exray_out *ray = m_d_exray_out_new(c, sharpness, maxdwell);
  if (! ray) {
    return 0;
  }
  char *bits = malloc(maxdwell + 2);
  if (! bits) {
    m_d_exray_out_delete(ray);
    return 0;
  }
  int n = 0;
  while (n <= maxdwell) {
    if (m_d_exray_out_have_bit(ray)) {
      bits[n++] = '0' + m_d_exray_out_get_bit(ray);
    }
    if (m_stepped != m_d_exray_out_step(ray)) {
      break;
    }
  }
  bits[n] = 0;
  m_d_exray_out_delete(ray);
  return bits;
}










static void usage(const char *progname) {
  fprintf
    ( stderr
    , "usage: %s precision c-re c-im sharpness maxdwell preperiod period\n"
    , progname
    );
}



// ================================= main =============================



extern int main(int argc, char **argv) {
  
  
  if (argc != 8) {
    usage(argv[0]);
    return 1;
  }
  
  
  
  
  
  bool native = true;
  int bits = 0;
  
  
  if (! arg_precision(argv[1], &native, &bits)) { return 1; }
  
  
  if (native) {
    double cre = 0;
    double cim = 0;
    int sharpness = 0;
    int maxdwell = 0;
    int preperiod = 0;
    int period = 0;
    if (! arg_double(argv[2], &cre)) { return 1; }
    if (! arg_double(argv[3], &cim)) { return 1; }
    if (! arg_int(argv[4], &sharpness)) { return 1; }
    if (! arg_int(argv[5], &maxdwell)) { return 1; }
    if (! arg_int(argv[6], &preperiod)) { return 1; }
    if (! arg_int(argv[7], &period)) { return 1; }
    if (! (preperiod >= 0)) { return 1; }
    if (! (period >= 0)) { return 1; }
    if (preperiod > 0 && ! (period > 0)) { return 1; }
    
    
    
    
    
    m_d_exray_out *ray = m_d_exray_out_new(cre + I * cim, sharpness, maxdwell);
    if (! ray) { return 1; }
    
    
    // for collecting bits of external angle 
    char *sbits = malloc(maxdwell + 2);
    if (! sbits) { m_d_exray_out_delete(ray); return 1; }
    
    
    
    //
    int n = 0;
    do {
      complex double c = m_d_exray_out_get(ray);
      printf("c = %.16e %.16e", creal(c), cimag(c));
      if (m_d_exray_out_have_bit(ray)) {
        int bit = m_d_exray_out_get_bit(ray);
        sbits[n++] = '0' + bit;
        printf(" bit = %d\n", bit);
      } else {
        printf("\n");
      }
    } while (m_stepped == m_d_exray_out_step(ray));
    //
    
    
    
    sbits[n] = 0;
    m_d_exray_out_delete(ray);
    
    
    
    // output
    if (preperiod + period > 0) {
      printf("\n.");
      for (int i = 0; i < preperiod && 0 <= n - 1 - i; ++i) {
        putchar(sbits[n - 1 - i]);
      }
      putchar('(');
      for (int i = preperiod; i < preperiod + period && 0 <= n - 1 - i; ++i) {
        putchar(sbits[n - 1 - i]);
      }
      putchar(')');
      putchar('\n');
    }
    
    
    
    free(sbits);
    return 0;
  } // if (native)
  
  else {
    fprintf(stderr, "non-double precision not supported yet\n");
  }
  return 1;
}






